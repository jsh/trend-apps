#!/bin/bash
# How many trends, on average are in a random sequence of length N?

die() { echo "$*" >&2 ; exit 1; }

set -e                                        # fail immediately on errors
set -u                                        # unset vars are errors
set -o pipefail                               # fails within pipes are errors

name=$(basename $1)                           # basenames for programs, configs
data_dir=$PWD/data
cfg_dir=$PWD/cfg
mkdir -p $data_dir                            # if it's not there, make it there
mkdir -p $cfg_dir                             # if it's not there, make it there

cfg_file=$cfg_dir/$name.cfg                   # and that of the config file
output=$data_dir/$name.data                   # where to put the output

# defaults
x_axis_label="X axis"
y_axis_label="Y axis"
title="Title"
transform() {
	cat                                   # default: pass the data through, un-transformed
}

[[ -f $cfg_file ]] && source $cfg_file

[[ -f $output ]] || die "missing file '$output'"

cat $output |
	transform |                           # transform axes
	plot_it.py "$x_axis_label" "$y_axis_label" "$title"
