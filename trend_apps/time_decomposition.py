#!/usr/bin/env python3

import time

import fire
from trends import _gen_random, decompose


def timeit(len: int, trials: int):
    """Total time to decompose 'trials' sequences of length 'len'."""
    total_time = 0
    for _ in range(trials):
        s_gen = _gen_random(nrands=len)
        tic = time.perf_counter()
        decompose(s_gen, direction="up")
        toc = time.perf_counter()
        total_time += toc - tic
    return total_time


def time_decomposition(
    max_length: int = 512, trials: int = 100, interval: int = -1
) -> None:
    """Average time to decompose random sequences of varying lengths into trends.

    max_length: int
        Maximum length sequence to decompose.

    trials: int
        Number of trials at each length.

    interval: int
        distance between sequence lengths. Default to interval that produces 16 points.
    """
    len = 0
    if interval == -1:
        interval = max_length // 16  # Goldilocks's "Just right."
    while len < max_length:
        print(len, timeit(len, trials))
        len += interval


if __name__ == "__main__":
    fire.Fire(time_decomposition)
