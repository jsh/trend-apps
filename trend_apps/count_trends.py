#!/usr/bin/env python3
"""Decompose a sequence into trends."""

import fire
from trends import random_trends


def ave_ntrends(sequence_length: int, trials: int) -> float:
    """Average number of trends.

    Random sequences of given length.
    Average over n repetitions.
    """
    total = 0
    for _ in range(trials):
        seq, _ = random_trends(sequence_length, direction="up")
        total += len(seq)
    return total / trials


def trends_by_seq_length(
    max_length: int = 10_000, trials: int = 100, interval: int = 2
) -> None:
    """Average number of trends by sequence length.

    max_length: int
        Maximum length sequence to decompose.

    trials: int
        Number of trials at each length.

    interval: int
        Amount to multiply by to get next length to try (default: 2).
    """
    length = 1

    while length < max_length:
        mean_ntrends = ave_ntrends(length, trials)
        print(length, mean_ntrends)
        length *= interval


if __name__ == "__main__":
    fire.Fire(trends_by_seq_length)
