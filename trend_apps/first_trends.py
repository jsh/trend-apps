#!/usr/bin/env python3

import fire
from trends import random_trends


def first_trend_length(length):
    uptrends, _ = random_trends(seq_length=length, direction="up")
    return uptrends[0].length


def ave_first_trend_length(length, trials):
    tot_lengths = 0
    for _ in range(trials):
        tot_lengths += first_trend_length(length)
    return tot_lengths / trials


def first_trend_by_seq_length(
    max_length: int = 4096, trials: int = 100, interval: int = 2
) -> None:
    """Average first trend length by sequence length.

    max_length: int
        Maximum length sequence to decompose.

    trials: int
        Number of trials at each length.

    interval: int
        Amount to multiply by to get next length to try (default: 2).
    """

    length = 1
    while length < max_length:
        print(length, ave_first_trend_length(length, trials))
        length *= interval


if __name__ == "__main__":
    fire.Fire(first_trend_by_seq_length)
