#!/bin/bash
# How many trends, on average are in a random sequence of length N?

set -e                                        # fail immediately on errors
set -u                                        # unset vars are errors
set -o pipefail                               # fails within pipes are errors

name=$(basename $1)                           # basenames for programs, configs
data_dir=$PWD/data
cfg_dir=$PWD/cfg
mkdir -p $data_dir                            # if it's not there, make it there
mkdir -p $cfg_dir                             # if it's not there, make it there

py_script=$name.py                            # infer the python script name
cfg_file=$cfg_dir/$name.cfg                   # and that of the config file
output=$data_dir/$name.data                   # where to put the output

# defaults
flags=""

[[ -f $cfg_file ]] && source $cfg_file

# if [[ ! -f $output ]]; then
./$py_script $flags > $output         # run the script
# fi
