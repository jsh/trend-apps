#!/usr/bin/env python3

from random import random

import fire
from trends import random_trends


def n_opposite_trends(length: int):
    """Generate a single, ascending trend and decompose in the opposite direction."""
    seed = random()
    uptrends, _ = random_trends(seq_length=length, direction="up", seed=seed)
    start_of_single_trend, _ = uptrends.rotate_to_single_trend()
    _, downtrends = random_trends(
        seq_length=length, direction="down", seed=seed, rot=start_of_single_trend
    )
    return len(downtrends)


def ave_opposite_trends(length: int, trials: int):
    """Find average number of opposite trends over 'trials' trials."""
    ntrends = 0
    for _ in range(trials):
        ntrends += n_opposite_trends(length)
    return ntrends / trials


def count_opposite_trends(
    max_length: int = 65536, trials: int = 100, interval: int = 2
) -> None:
    """Average number of opposite trends by sequence length.

    max_length: int
        Maximum length sequence to decompose.

    trials: int
        Number of trials at each length.

    interval: int
        Amount to multiply by to get next length to try (default: 2).
    """

    length = 1
    while length < max_length:
        mean_ntrends = ave_opposite_trends(length, trials)
        print(length, mean_ntrends)
        length *= interval


if __name__ == "__main__":
    fire.Fire(count_opposite_trends)
