#!/usr/bin/env python3

import fire
from trends import random_trends


def longest_trend(length):
    uptrends, _ = random_trends(seq_length=length, direction="up")
    longest_trend = max(uptrends.lengths(), default=0)
    return longest_trend


def ave_longest_trend(length, trials):
    tot = 0
    for _ in range(trials):
        tot += longest_trend(length)
    return tot / trials


def longest_trends(max_len: int = 16384, trials: int = 100, interval: int = -1) -> None:
    """Longest trends from random sequences of varying lengths.

    max_len: int
        Maximum length sequence to decompose.

    trials: int
        Number of trials at each length.

    interval: int
        distance between sequence lengths. Default to interval that produces 16 points.
    """
    len = 0
    if interval == -1:
        interval = max_len // 16  # Goldilocks's "Just right."
    while len < max_len:
        print(len, ave_longest_trend(len, trials))
        len += interval


if __name__ == "__main__":
    fire.Fire(longest_trends)
