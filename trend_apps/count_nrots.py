#!/usr/bin/env python3
"""Decompose a sequence into trends."""

import fire
from trends import random_trends


def ave_nrots(sequence_length: int, trials: int) -> float:
    """Average number of rotations_to_single_trend.

    Random sequences of given length.
    Average over n repetitions.
    """
    total_nrots = 0
    for _ in range(trials):
        seq, _ = random_trends(sequence_length, direction="up")
        (pos, nrots) = seq.rotate_to_single_trend()
        total_nrots += nrots
    return total_nrots / trials


def rotations_by_seq_length(
    max_length: int = 10_000, trials: int = 100, interval: int = 2
) -> None:
    """Average number of rotations by sequence length to get to single trend.

    max_length: int
        Maximum length sequence to decompose.

    trials: int
        Number of trials at each length.

    interval: int
        Amount to multiply by to get next length to try (default: 2).
    """
    length = 1

    while length < max_length:
        mean_nrots = ave_nrots(length, trials)
        print(length, mean_nrots)
        length *= interval


if __name__ == "__main__":
    fire.Fire(rotations_by_seq_length)
