#!/usr/bin/env python3

import sys

import pandas as pd
import plotly.express as px  # type: ignore[import]


def scatter_plot(x_list, y_list, x_label="x axis", y_label="y axis", title="Title"):
    """Scatter plot (x, y)."""
    dataframe = pd.DataFrame({x_label: x_list, y_label: y_list})
    fig = px.scatter(dataframe, x=x_label, y=y_label, trendline="ols", title=title)
    fig.show()


usage = f"usage: {sys.argv[0]} [x_label [y_label [title]]]"
if sys.argv[1] == "--help":
    sys.exit(usage)

x_list = []
y_list = []

for line in sys.stdin:
    x, y = line.strip().split()
    x_list.append(float(x))
    y_list.append(float(y))
if len(sys.argv) == 1:
    scatter_plot(x_list, y_list)
elif len(sys.argv) == 2:
    scatter_plot(x_list, y_list, x_label=sys.argv[1])
elif len(sys.argv) == 3:
    scatter_plot(x_list, y_list, x_label=sys.argv[1], y_label=sys.argv[2])
elif len(sys.argv) == 4:
    scatter_plot(x_list, y_list, x_label=sys.argv[1], y_label=sys.argv[2], title=sys.argv[3])
else:
    sys.exit(usage)
