{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "64698775",
   "metadata": {},
   "source": [
    "# Using the `trends` Package"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b086dc4c",
   "metadata": {},
   "source": [
    "`trends` is a Python package that lets you create, manipulate, and explore trends.\n",
    "This notebook will show you how to use it, with half a dozen examples.\n",
    "\n",
    "(If you aren't familiar with trends at all, you may want to start by working through the Trends_Tutorial notebook.)\n",
    "\n",
    "If you want to look at the source, you can clone it with `git clone https://gitlab.com/jsh/trends.git`,\n",
    "but a ready-to-go package, `trends`, is already in the Python Package Index, PyPI, so you can install it directly like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6ee7884c",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "pip install trends"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da38fd9a",
   "metadata": {},
   "source": [
    "When it's installed, you can just import and use it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6084b17a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import trends"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb3006ad",
   "metadata": {},
   "source": [
    "## How fast can you decompose a random sequence into trends?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4c63089",
   "metadata": {},
   "source": [
    "### Brute force is slow."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d879547",
   "metadata": {},
   "source": [
    "In the trends tutorial, you decomposed random sequences into maximal trends by finding the longest prefix that was a trend, then doing that again on the remaining suffix, and continuing until the sequence was completely decomposed,\n",
    "like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3be8822d",
   "metadata": {},
   "outputs": [],
   "source": [
    "import statistics\n",
    "\n",
    "# figure out if a sequence is a trend\n",
    "def is_trend(seq):\n",
    "    n = len(seq)\n",
    "    for i in range(1,n):\n",
    "        if statistics.mean(seq[:i]) > statistics.mean(seq[i:]):\n",
    "            return False\n",
    "    return True\n",
    "\n",
    "# find the first trend in a larger sequence\n",
    "def pfx_trend(seq):\n",
    "    cpy = seq.copy()\n",
    "    # starting with the whole sequence,\n",
    "    # back up, looking at each prefix\n",
    "    # until you find a trend\n",
    "    while cpy:\n",
    "        if is_trend(cpy):\n",
    "            return cpy\n",
    "        cpy.pop()\n",
    "        \n",
    "# find all the trends, first to last\n",
    "def decompose(seq):\n",
    "    trendlist = []\n",
    "    while seq:\n",
    "        pfx = pfx_trend(seq)\n",
    "        trendlist.append(pfx)\n",
    "        pfx_len = len(pfx)\n",
    "        seq = seq[pfx_len:]\n",
    "    return trendlist"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a495129",
   "metadata": {},
   "outputs": [],
   "source": [
    "from random import random\n",
    "seq = [random() for _ in range(10)]\n",
    "decompose(seq)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a654033",
   "metadata": {},
   "source": [
    "How fast is that? Just measure it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "184d14d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "from random import random\n",
    "\n",
    "def decompose_time(nrands):\n",
    "    seq = [random() for _ in range(nrands)]         # make a list of nrand, random floats\n",
    "    tic = time.perf_counter()\n",
    "    decompose(seq)                                  # the original version\n",
    "    toc = time.perf_counter()\n",
    "    return toc - tic\n",
    "\n",
    "decompose_time(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7eb7a5a",
   "metadata": {},
   "source": [
    "Not bad. What about a longer sequence, say 10 times as long?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13e1802c",
   "metadata": {},
   "outputs": [],
   "source": [
    "decompose_time(100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c18e197",
   "metadata": {},
   "source": [
    "Lots slower! Maybe you just got unlucky, and it was a particularly hard-to-decompose random sequence."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76611b36",
   "metadata": {},
   "outputs": [],
   "source": [
    "def mean_decompose_time(nrands, trials):\n",
    "    total_time = 0\n",
    "    _ = decompose_time(nrands) # throw one away, in case there's a significant startup cost\n",
    "    for _ in range(trials):\n",
    "        total_time += decompose_time(nrands)\n",
    "    return round(total_time / trials, 4)           # time to four decimal places\n",
    "\n",
    "mean_decompose_time(100, 100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d5fd41ee",
   "metadata": {},
   "source": [
    "Hmm. Time to look at how time changes with sequence length. You know: the big-O stuff."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f90ca7cb",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def length_vs_time(max, trials, npoints):\n",
    "    increment = max // (npoints)\n",
    "    t_vs_l = {}\n",
    "    for nrands in range(0, max, increment):\n",
    "        t_vs_l[nrands] = mean_decompose_time(nrands, trials)\n",
    "    return t_vs_l\n",
    "\n",
    "lvt = length_vs_time(550, 100, 10)\n",
    "lvt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f119071",
   "metadata": {},
   "source": [
    "Even without a graph, you can see that's not linear, and quickly becomes slower than a herd of turtles.\n",
    "Here's how to graph it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c9ef02d",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "x = lvt.keys()   # sequence length\n",
    "y = lvt.values() # time to decompose\n",
    "plt.plot(x, y)\n",
    "plt.title(\"Decomposition Time (Brute Force)\")\n",
    "plt.xlabel(\"Sequence Length\")\n",
    "plt.ylabel(\"Seconds\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83205dc5",
   "metadata": {},
   "source": [
    "Yow. Looks almost exponential. Is it? If so, taking logs of the times will straighten the graph out."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c45c9dcd",
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "log_y = [math.log(elem+1) for elem in y]\n",
    "# (Add $1$ to $y$ so that you don't have to take $log(0)$ for the first point.)\n",
    "plt.plot(x, log_y)\n",
    "plt.title(\"Decomposition Time (Brute Force)\")\n",
    "plt.xlabel(\"ln(Sequence Length)\")\n",
    "plt.ylabel(\"Seconds\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e93dc14d",
   "metadata": {},
   "source": [
    "Looks like the shape of the curve doesn't even change much.\n",
    "\n",
    "This algorithm's so slow that it's not practical for studying long sequences."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3aa1c140",
   "metadata": {},
   "source": [
    "### The `trends` package decomposes *fast!*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0faf24de",
   "metadata": {},
   "source": [
    "Fortunately, the `trends` package has a better-behaved algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5230e1c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import trends\n",
    "\n",
    "def decompose_time(nrands):\n",
    "    rand_list = [random() for _ in range(nrands)]         # make a list of nrand, random floats\n",
    "    tic = time.perf_counter()\n",
    "    trends.decompose(rand_list)                           # the trends package version\n",
    "    toc = time.perf_counter()\n",
    "    return toc - tic\n",
    "\n",
    "def mean_decompose_time(nrands, trials):\n",
    "    total_time = 0\n",
    "    _ = decompose_time(nrands)\n",
    "    for _ in range(trials):\n",
    "        total_time += decompose_time(nrands)\n",
    "    return round(total_time / trials, 4)           # time to four decimal places\n",
    "\n",
    "def length_vs_time(max, trials, npoints):\n",
    "    increment = max // (npoints)\n",
    "    t_vs_l = {}\n",
    "    for nrands in range(0, max, increment):\n",
    "        t_vs_l[nrands] = mean_decompose_time(nrands, trials)\n",
    "    return t_vs_l"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ff77216f",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "lvt = length_vs_time(2_000, 100, 10)  # nearly four times as many as with the earlier algorithm.\n",
    "lvt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1a02822",
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "x = lvt.keys()\n",
    "y = lvt.values()\n",
    "plt.plot(x, y)\n",
    "plt.title(\"Decomposition Time (trends.decompose())\")\n",
    "plt.xlabel(\"Sequence Length\")\n",
    "plt.ylabel(\"Seconds\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65356623",
   "metadata": {},
   "source": [
    "It's quite linear: $O(N)$. On this laptop, the mean time to decompose a random sequence of 1_000 floats into trends is 4 milliseconds, so it should be able to decompose a sequence of a *million*, random floats into trends in about 4 seconds and twice that much in just twice the time.\n",
    "\n",
    "That's a million random floats in less time than it took the other algorithm took to do five hundred."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9d4d4dab",
   "metadata": {},
   "outputs": [],
   "source": [
    "length_vs_time(2_000_000, 1, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8745c141",
   "metadata": {},
   "source": [
    "Peppy. You can work with that."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f6409270",
   "metadata": {},
   "source": [
    "## How Many Trends in a Random Sequence?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54be10f6",
   "metadata": {},
   "source": [
    "That was fun. What else could you use `trends` to ask?\n",
    "How about the average number of trends in a random sequence.\n",
    "\n",
    "Again, this will depend on sequence length -- you'd expect longer sequences to usually have more trends -- \n",
    "and although the result doesn't seem obvious,\n",
    "the code isn't hard.\n",
    "\n",
    "`trends.decompose()` returns a two-element tuple.\n",
    "For now, you'll just need the first, a list of the trends in the sequence. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4534158",
   "metadata": {},
   "outputs": [],
   "source": [
    "def ntrends(seq):\n",
    "    trendlist = trends.decompose(seq)[0]\n",
    "    return len(trendlist)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29eb4d64",
   "metadata": {},
   "source": [
    "Does it work?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9fe7b9ab",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "s_up = [0.1, 0.2, 0.4]    # an increasing trend\n",
    "print(ntrends(s_up))\n",
    "s_down = [0.4, 0.2, 0.1]  # the decreasing trend `list(reversed(s.up))`\n",
    "print(ntrends(s_down))\n",
    "s_mixed = [0.4, 0.1, 0.2] # neither increasing nor decreasing\n",
    "print(ntrends(s_mixed))\n",
    "s_random = [random() for _ in range(20)] # a sequence of random floats\n",
    "print(s_random)\n",
    "print(ntrends(s_random))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac94e5fb",
   "metadata": {},
   "source": [
    "Try some yourself!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1881bd81",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "772b4228",
   "metadata": {},
   "source": [
    "The next steps are easy, because they're analogous to the ones you used to time decomposition.\n",
    "First, the function to find the mean number of trends for a fixed length sequence over a number of trials."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c33c6bf",
   "metadata": {},
   "outputs": [],
   "source": [
    "def mean_ntrends(nrands, trials):\n",
    "    total_trends = 0\n",
    "    for _ in range(trials):\n",
    "        s_random = [random() for _ in range(nrands)]\n",
    "        total_trends += ntrends(s_random)\n",
    "    return round(total_trends / trials, 3)           # average number of trends to three decimal places"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8896ace2",
   "metadata": {},
   "source": [
    "Trying it out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "219edca7",
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_ntrends(20, 100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4bad56b4",
   "metadata": {},
   "source": [
    "Next, wrap that in another function, which prints the mean at a variety of sequence lengths."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64b43b40",
   "metadata": {},
   "outputs": [],
   "source": [
    "def trends_by_length(max, trials, npoints):\n",
    "    increment = max // (npoints)\n",
    "    ntrends = {}\n",
    "    for nrands in range(0, max, increment):\n",
    "        ntrends[nrands] = mean_ntrends(nrands, trials)\n",
    "    return ntrends"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a253534",
   "metadata": {},
   "outputs": [],
   "source": [
    "trends_by_length(100_000, 100, 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24063476",
   "metadata": {},
   "source": [
    "Unfortunately, that doesn't look linear. Not as lucky this time.\n",
    "\n",
    "No reason to be discouraged. It turns out you can get a better line with a couple of simple transformations:\n",
    "    \n",
    "* Instead of using $string\\ length$ for the X axis, use $ln(string\\ length)$.\n",
    "* Instead of using the average number of trends for the Y axis, use the average number of trend *restarts* -- the number of times a trend ends before the end of the sequence and a new one starts.\n",
    "    \n",
    "Think of $(ntrends - 1)$ as the number of *hiccups*. If the whole sequence is a single trend, there are no hiccups; if the sequence decomposes into two trends, there's one hiccup; and so on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "07fed127",
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "\n",
    "def hiccups_by_log_length(max, trials):\n",
    "    hiccups = {}\n",
    "    nrands = 1         # start small\n",
    "    while nrands <= max:\n",
    "        log_length = round(math.log(nrands), 2)\n",
    "        mean_hiccups = mean_ntrends(nrands, trials) - 1\n",
    "        hiccups[log_length] = mean_hiccups\n",
    "        nrands *= 2    # double nrands at each iteration\n",
    "    return hiccups"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a6ed3c81",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "hiccups = hiccups_by_log_length(100_000, 100)\n",
    "hiccups"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b42a9817",
   "metadata": {},
   "source": [
    "Now graph it to see how good the straight line is.\n",
    "\n",
    "You've made quick-and-dirty graphs already, but\n",
    "you're going to be graphing a lot of things from here on out,\n",
    "so why not take a minute to build functions to do a nicer job that you can re-use?\n",
    "\n",
    "Build it a piece at a time.\n",
    "First off, a simple scatter plot of the data points,\n",
    "with labeled axes and the title."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4afca82c",
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "def graph(x, y, x_label, y_label, title):    \n",
    "    # Create a scatterplot with a title and axis labels\n",
    "    plt.figure(figsize=(10, 10))\n",
    "    plt.scatter(x, y, s=10, alpha=0.7)\n",
    "    plt.title(title)\n",
    "    plt.xlabel(x_label)\n",
    "    plt.ylabel(y_label)\n",
    "    \n",
    "log_len = list(hiccups.keys())\n",
    "hics = list(hiccups.values())\n",
    "\n",
    "graph(log_len, hics, \n",
    "      x_label=\"ln(Sequence Length)\",\n",
    "      y_label=\"Average Number of Hiccups\",\n",
    "      title=\"Hiccups as a Function of Sequence Length\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3637050c",
   "metadata": {},
   "source": [
    "Next, draw a best-fit, least-squares line, \n",
    "and add a legend with the equation of the line, \n",
    "plus the coefficient of determination to measure goodness-of-fit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08ed205e",
   "metadata": {},
   "outputs": [],
   "source": [
    "def slope_and_intercept(x, y):\n",
    "    # fit a least-squares line\n",
    "    m, b = np.polyfit(x, y, deg=1)\n",
    "    # coefficient of determination = correlation ** 2\n",
    "    return (m, b)\n",
    "\n",
    "def legend(m, b, r):\n",
    "    equation = f\"y = {m:2.2f}*x + {b:2.2f}\"\n",
    "    rho_sq = \"\\u03c1**2\"    # Unicode character for rho is U+03C1. Sadly, the font lacks superscripts.\n",
    "    goodness_of_fit = f\"{rho_sq} = {r**2:2.3f}\"\n",
    "    legend = f\"{equation}\\n{goodness_of_fit}\"\n",
    "    return legend\n",
    "\n",
    "def annotate_graph(x, y):\n",
    "    # add in a best-fit line and a legend\n",
    "    m, b = slope_and_intercept(x, y) \n",
    "    r = np.corrcoef(x, y)[0,1]\n",
    "\n",
    "    # create nparray that spans the x-space\n",
    "    xseq = np.linspace(0, math.ceil(x[-1]), num=100)\n",
    "\n",
    "    # create a legend\n",
    "    graph_legend = legend(m, b, r)\n",
    "\n",
    "    # add best-fit line and legend to the plot\n",
    "    plt.plot(xseq, m * xseq + b, color=\"red\", lw=1.5) # best-fit line\n",
    "    plt.text(0, y[-2], graph_legend, color=\"red\")           # legend in the urh corner\n",
    "        \n",
    "annotate_graph(log_len, hics)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56adc73a",
   "metadata": {},
   "source": [
    "Finally, put them together into a single figure!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5bfb68a7",
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "def graph(x, y, x_label, y_label, title):    \n",
    "    # Create a scatterplot with a title and axis labels\n",
    "    plt.figure(figsize=(10, 10))\n",
    "    plt.scatter(x, y, s=10, alpha=0.7)\n",
    "    plt.title(title)\n",
    "    plt.xlabel(x_label)\n",
    "    plt.ylabel(y_label)\n",
    "    annotate_graph(x, y) # add the legend and the best-fit line\n",
    "    \n",
    "log_len = list(hiccups.keys())\n",
    "hics = list(hiccups.values())\n",
    "\n",
    "graph(log_len, hics, \n",
    "      x_label=\"ln(Sequence Length)\",\n",
    "      y_label=\"Average Number of Hiccups\",\n",
    "      title=\"Hiccups as a Function of Sequence Length\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c78dc3f",
   "metadata": {},
   "source": [
    "A remarkable fit! \n",
    "\n",
    "To an engineering approximation the average number of hiccups is the natural log of the sequence length.\n",
    "\n",
    "To convince yourself this isn't a fluke, go back and re-execute the function call, followed by the plot, a few times, choosing whatever values you like for the parameters to `hiccups_by_log_length()`.\n",
    "\n",
    "The position of the points will change, but the slope will remain about 1, the intercept will continue to hug zero, and the fit to a line will stay excellent.\n",
    "\n",
    "This seems surprising. It's not a shock that *some* function connects the average number of hiccups to sequence length, or even that the growth is $O(ln(N))$.  \n",
    "\n",
    "Still, they're not just proportional to one another, once you get away from tiny values of $N$, the relationship is simple: one appears to be exactly the natural log of the other!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d213e34d",
   "metadata": {},
   "source": [
    "## What's the Distribution of Numbers of Hiccups?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6bd91e94",
   "metadata": {},
   "source": [
    "Now that you know the average number of hiccups is about $ln(N)$, what's the distribution of number-of-hiccups look like? A sequence of length two million will have about 15, because $ln(2*10^{6}) \\approx 14.5$ But how many sequences that length will have about 10? About 20?\n",
    "\n",
    "For that, you need to look at a measure of spread. A pretty common one is the variance, $\\sigma^{2}$.\n",
    "\n",
    "It's not hard to get that number with the standard, Python module, `statistics`, which supplies both `mean()` and `variance()` functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5376f4c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "import collections\n",
    "from random import random\n",
    "from statistics import mean, variance\n",
    "\n",
    "import trends\n",
    "\n",
    "def basic_stats(lst):\n",
    "    # return mean and variance of a list\n",
    "    Stats = collections.namedtuple(\"Stats\", \"mean variance\")\n",
    "    return Stats(mean = f\"{mean(lst):2.2f}\", variance = f\"{variance(lst):2.2f}\")\n",
    "\n",
    "def trendlist_hiccups(nrands, trials):\n",
    "    # generate a lot of trendlists of a given length\n",
    "    # count the hiccups in each\n",
    "    # and report the mean and variance of that count\n",
    "    trendlist_hics = []\n",
    "    for _ in range(trials):\n",
    "        seq = [random() for _ in range(nrands)]\n",
    "        trendlist_hics.append(len(trends.decompose(seq)[0]) - 1)\n",
    "    return trendlist_hics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9be64d05",
   "metadata": {},
   "outputs": [],
   "source": [
    "hiccup_data = trendlist_hiccups(1_000_000, 2_000)\n",
    "basic_stats(hiccup_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "601a5551",
   "metadata": {},
   "source": [
    "The mean and variance are remarkably similar. That should ring a bell.\n",
    "\n",
    "The Poisson distribution is a common, discrete distribution with the interesting property that $\\mu = \\sigma^{2}$\n",
    "Perhaps lengths are Poisson distributed!\n",
    "\n",
    "Before testing this, spend a second thinking about what that means.\n",
    "\n",
    "The equation for the Poisson is $P(N, \\lambda) = e^{-\\lambda}\\lambda^{N}/N!$ , \n",
    "where $N$ is the number of events (in this case, hiccups), and $\\lambda$ is the mean (or, since they're the same, the variance).\n",
    "\n",
    "In other words, for sequences of length two million, you know the average number of hiccups will be $\\lambda = ln(2*10^{6}) = 14.5$, but you also expect the probability of 10 hiccups to be $P(10, \\lambda)$, the probabilit of 20 to be $P(20, \\lambda)$, and so on, with a simple formula for $P$ in each case.\n",
    "\n",
    "In particular, the probability of *no* hiccups is \n",
    "$P(0, \\lambda) = e^{-\\lambda}\\lambda^{0}/0! \n",
    "= e^{-\\lambda}\n",
    "= 1/e^{\\lambda}\n",
    "= 1/e^{ln(N)}$\n",
    "= 1/N\n",
    "\n",
    "Which you already know is correct. Every random sequence has exactly one circular permutation that is a single trend."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53113c8d",
   "metadata": {},
   "source": [
    "That's certainly encouraging."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb762f01",
   "metadata": {},
   "source": [
    "## How Well Do the Number-of-Trends Fit a Poisson?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d5cc0d29",
   "metadata": {},
   "source": [
    "So, is it a Poisson? Why not look at the data and a Poisson, side-by-side. First, the observations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e686eb7f",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# initialize arrays to store frequencies\n",
    "domain = max(hiccup_data) + 1\n",
    "observed = [0]*domain\n",
    "# now populate the array\n",
    "for hics in hiccup_data:\n",
    "    observed[hics] += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c154a1cf",
   "metadata": {},
   "source": [
    "Now, the expected frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f1826cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import exp, factorial\n",
    "\n",
    "def poisson(k, mu):\n",
    "    return exp(-mu)*(mu**k)/factorial(k)\n",
    "expected = [0]*domain\n",
    "for hics in range(domain):\n",
    "    expected[hics] = poisson(hics, math.log(1_000_000))*sum(observed)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36c69c36",
   "metadata": {},
   "source": [
    "For a chi-squared test $sum(observed)$ must equal $sum(expected)$, so we'll cheat and force that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2e100806",
   "metadata": {},
   "outputs": [],
   "source": [
    "expected[-1] += sum(observed) - sum(expected)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14c151d1",
   "metadata": {},
   "source": [
    "You can see these, side-by-side, here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "454b8c34",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "#The data\n",
    "indices = range(domain)\n",
    "\n",
    "#Calculate optimal width\n",
    "width = np.min(np.diff(indices))/3\n",
    "\n",
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(111)\n",
    "# matplotlib 3.0 you have to use align\n",
    "ax.bar(indices-width,expected, width,color=\"b\", align=\"edge\", label=\"expected (Poisson)\", alpha=0.5)\n",
    "ax.bar(indices,observed,width, color=\"r\", align=\"edge\", label=\"observed (hiccups)\")\n",
    "\n",
    "\n",
    "ax.set_xlabel(\"Number of Hiccups\")\n",
    "ax.set_ylabel(\"Frequency\")\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce4305e9",
   "metadata": {},
   "source": [
    "Looks fairly close, but not quite. Slightly too many trend-poor sequences, slightly too few that are trend-rich.\n",
    "\n",
    "You might not expect the fit to a Poisson to be perfect, for a couple of reasons:\n",
    "\n",
    "1. A Poisson has no upper bound on events ($k$).\n",
    "The number of hiccups can't be greater than the number of elements in the sequence;\n",
    "The variance of the observations should be smaller than the variance of the corresponding Poisson.\n",
    "1. That's actually what you see. The variance is smaller than the mean.\n",
    "\n",
    "Indeed, a chi-squared test fails."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa5dccb5",
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.stats as stats\n",
    "import numpy as np  \n",
    "  \n",
    "# Chi-Square Goodness of Fit Test\n",
    "chi_square_test_statistic, p_value = stats.chisquare(observed, expected)\n",
    "  \n",
    "# chi square test statistic and p value\n",
    "print('chi_square_test_statistic is : ' +\n",
    "      str(chi_square_test_statistic))\n",
    "print('p_value : ' + str(p_value))\n",
    "  \n",
    "  \n",
    "# find Chi-Square critical value\n",
    "print(stats.chi2.ppf(1-0.05, df=domain))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d28dd072",
   "metadata": {},
   "source": [
    "Barely a Poisson, and with smaller numbers it's farther away still. Maybe a binomial or beta-binomial distribution will fit better, but for now, we'll move on.\n",
    "\n",
    "It's close enough, though, that you can use the similarity to condition your intuition. Something in the neighborhood of $P(k,ln(N))$ sequences of length $N$ will have exactly $k$ hiccups. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6678cf9b",
   "metadata": {},
   "source": [
    "## How Fast Can You Find the Circular Permutation That's a Single Trend?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7e4b344b",
   "metadata": {},
   "source": [
    "Every sequence has exactly one circular permutation that's a single trend. How fast can you find it?\n",
    "\n",
    "The brute-force technique would be to rotate it one, decompose it, if that's a single trend, you're done.\n",
    "If not, go back and rotate it by two instead, decompose that, look to see if it's a single trend, and continue like that until you find the answer.\n",
    "\n",
    "Decomposition is $O(N)$, so this algorithm would be $O(N^{2})$.\n",
    "\n",
    "The `trends` package also makes this task $O(N)$.\n",
    "\n",
    "Watch!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e23c6b5c",
   "metadata": {},
   "source": [
    "Suppose you've decompose a sequence into trends. What would happen if you popped the leftmost trend off the end, and stuck it on the right end of what remained -- turned the prefix into a suffix?\n",
    "\n",
    "In the original decomposition, the trend means decreased monotonically, from left to right. (If you need to see why, check out the trends tutorial.) This means that the first trend's mean is greater than the last trend's. When you pop it off the beginning and stick it on the end, it will fuse with the old last trend. The new sequence will have at least one fewer trend. \n",
    "\n",
    "Keep rotating trends from the beginning to the end and you'll eventually get to a single trend.\n",
    "\n",
    "But why \"at least one fewer\" instead of just \"one fewer\"? That fused trend will have a bigger mean than the old last trend -- maybe so big that it will fuse with the one to its left, and so on. The first trend's mean could be so enormous that rotating it to the end would produce a single trend!\n",
    "\n",
    "You wouldn't expect this to happen often with random sequnces, but it could happen.\n",
    "\n",
    "So, rotating trends will produce a single trend, but that could take lots of rotations, or perhaps just one.\n",
    "What's the average number of rotations?\n",
    "\n",
    "Even if the answer doesn't leap out at you, writing the code's easy."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bde16764",
   "metadata": {},
   "source": [
    "The function `trends.decompose(s: List[float])` decomposes a list of floats into a `Trendlist` -- a list of `Trend` objects. The method `Trendlist.rotate_to_single_trend()` rotates the `Trend` objects in that list, merging trends whenever it can, and returns the number of rotations needed to get to a single trend."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fadf14fe",
   "metadata": {},
   "outputs": [],
   "source": [
    "import trends\n",
    "from random import random\n",
    "s_rand = [round(random(),3) for _ in range(100)]\n",
    "print(s_rand)\n",
    "tl_rand = trends.decompose(s_rand)[0]\n",
    "print(len(tl_rand))\n",
    "print(tl_rand.rotate_to_single_trend())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "665ab088",
   "metadata": {},
   "source": [
    "The call returns the tuple `(position, number_of_rotations)`, where `position` is the sequence element that ends up first in the rotated sequence. The number we want is element `rotate_to_single_trend()[1]`\n",
    "\n",
    "We'll use the same pattern. Here's the number of rotations for a random sequence of a given length:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ea320690",
   "metadata": {},
   "outputs": [],
   "source": [
    "def mean_nrots(nrands, trials):\n",
    "    total_rotations = 0\n",
    "    for _ in range(trials):\n",
    "        s_random = [random() for _ in range(nrands)]\n",
    "        trendlist = trends.decompose(s_random)[0]\n",
    "        total_rotations += trendlist.rotate_to_single_trend()[1]\n",
    "    return round(total_rotations / trials, 3)           # average number of rotations to three decimal places"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eab58491",
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_nrots(1_000, 100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fa65f6eb",
   "metadata": {},
   "source": [
    "And the number of rotations as a function of sequence length."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f2280b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "\n",
    "def rotations_by_log_length(max, trials):\n",
    "    rotations = {}\n",
    "    nrands = 1         # start small\n",
    "    while nrands <= max:\n",
    "        log_length = round(math.log(nrands), 2)\n",
    "        mean_rotations = mean_nrots(nrands, trials)\n",
    "        rotations[log_length] = mean_rotations\n",
    "        nrands *= 2    # double nrands at each iteration\n",
    "    return rotations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc2f1b3f",
   "metadata": {},
   "source": [
    "Use the log of the sequence length again. After all, it worked well last time. What the heck."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0613b8c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "rotations = rotations_by_log_length(10_000, 100)\n",
    "rotations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a48a17b6",
   "metadata": {},
   "source": [
    "Notice that there's no reason to subtract 1 from `mean_nrots()`, the way you did for `mean_ntrends()`.\n",
    "After all, this `sets rotations[0] == 0`, which we want."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14715713",
   "metadata": {},
   "source": [
    "Once again, let's graph the result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd435b86",
   "metadata": {},
   "outputs": [],
   "source": [
    "log_len = list(rotations.keys())\n",
    "rots = list(rotations.values())\n",
    "\n",
    "graph(log_len, rots,\n",
    "    x_label=\"ln(Sequence Length)\",\n",
    "    y_label=\"Average Number of Rotations\",\n",
    "    title=\"Rotations to Single Trend as a Function of Sequence Length\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3fc7bfe",
   "metadata": {},
   "source": [
    "See? It *was* worth writing a re-usable graphing function.\n",
    "\n",
    "Using the log of the length was a good choice this time, too. This is another fine fit.\n",
    "\n",
    "Comparing this graph to the previous one, you can see that the average number of rotations to get to a single trend,\n",
    "once it's decomposed, is about half the average number of trends.\n",
    "\n",
    "That make intuitive sense. If any of the $ln(nrands)$ trends could start the single trend, the average number of rotations should be about half that.\n",
    "\n",
    "Like decomposition, finding the rotation that produces a single trend is also $O(N)$, since it can be done in two steps:\n",
    "\n",
    "1. a decomposition that's $O(N)$\n",
    "2. a set of rotations that is only $O(ln(N))$\n",
    "\n",
    "Summing these makes the whole algorithm $O(N)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e78afff9",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
