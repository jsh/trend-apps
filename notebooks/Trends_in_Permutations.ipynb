{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "35577c60",
   "metadata": {},
   "source": [
    "# Trends in Permutations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "606bce85",
   "metadata": {},
   "source": [
    "A trend is a number sequence with one interesting property:\n",
    "if you put your finger between any two numbers in that sequence, breaking it into a prefix and a suffix,\n",
    "the average of the prefix is smaller than the average of the suffix.\n",
    "\n",
    "In a trend, things are always \"looking up.\"\n",
    "\n",
    "A fun way to explore trends is to pick a sequence, then decompose each permutation of that sequence into trends.\n",
    "\n",
    "This notebook explores that."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b562f65",
   "metadata": {},
   "source": [
    "## Unique Decomposition"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84f922f0",
   "metadata": {},
   "source": [
    "You can decompose nearly every list, uniquely, into maximum-length trends.\n",
    "When you do, the average of those trends drops, steadily, from left-to-right.\n",
    "After all, if a trend mean is less than the mean of the trend to its immediate right,\n",
    "then the two form a single, longer trend.\n",
    "\n",
    "For example, [32, 4, 1, 16, 2, 8] decomposes into [32] [4, 1, 16] [2, 8]. Each subsequence is a trend, and the means of the trends -- [32, 7, 5] -- drops from left to right. \n",
    "\n",
    "Some sequences won't decompose uniquely. How would you decompose [1, 1, 1, 1, 1]? \n",
    "\n",
    "* As a single trend, [1, 1, 1, 1, 1]? \n",
    "* As two, [1,1] [1, 1, 1]? \n",
    "* As five, [1], [1], [1], [1], [1]?\n",
    "\n",
    "For unique decomposition, you just need a couple of conditions.\n",
    "\n",
    "1. **in-between-ness**\n",
    "If you break a sequence in two, the two parts must have averages that flank the average of the whole;\n",
    "that is, if A and B are two sub-sequences, and A+B is their concatenation, then average(A) > average(B) \n",
    "implies average(A) > average(A+B) > average(B). Any average with this property will work. \n",
    "\n",
    "1. **uniqueness** No two trends can have the same average: if they do, the order of the two trends in the decomposition is ambiguous.\n",
    "\n",
    "The arithmetic mean, $\\mu$, has in-between-ness, but so do the geometric mean, the harmonic mean, and many other measures of central tendency.\n",
    "\n",
    "The mode won't! if $A = {1, 1, 2, 2, 2, 3}$ and $B = {1, 1, 4, 4, 4, 5}$\n",
    "then $mode(A) == 2$ and $mode(B) == 4$, but $mode(A+B) == 1$\n",
    ".\n",
    "\n",
    "The median routinely runs afoul of the second condition.\n",
    "If $A = {1, 2, 2}$ and B = {2, 2, 3}$ then $median(A) == median(B) == 2$.\n",
    "\n",
    "\n",
    "First, notice that if every subset of a set of numbers has a different average, \n",
    "then any sequence of those numbers has a unique decomposition. \n",
    "No matter how you order the numbers, no two trends in their decomposition can wind up with the same average.\n",
    "\n",
    "If you can assure that, adjacent trends in *any* permutation of those numbers can never have the same average. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39264678",
   "metadata": {},
   "source": [
    "Lets call an iterable where you can uniquely decompose any permutation, ***trendy***."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2ce595c",
   "metadata": {},
   "source": [
    "I think two kinds of sequences will satisfy both conditions."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3040c12",
   "metadata": {},
   "source": [
    "$\\color{red}{\\text{A proof would be nice, but I don't have one yet.}}$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8cf2adba",
   "metadata": {},
   "source": [
    "1. Sets of prime powers: sets like ${1, 5, 25, 125, ..., 5^k}$\n",
    "1. Sets of random reals\n",
    "\n",
    "`trendlist.pows()` will generate the first, and `trendlist.rands()`, the second.\n",
    "\n",
    "We'll stick to these sets and to the arithmetic mean, which is familiar and easy to compute."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5285eb92",
   "metadata": {},
   "source": [
    "Next, let's move on to counting the number of trends in every permutation of a trendy iterable.\n",
    "This seems like it might be a problem in enumerative combinatorics, but it's not obvious how to attack it.\n",
    "\n",
    "Let's explore a little, to see if we can get ideas by writing some code."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28cdf372",
   "metadata": {},
   "source": [
    "## Counting Trends in Permutations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6852c20",
   "metadata": {},
   "source": [
    "The set ${1, 2, 4}$, has $3!$ possible permutations.\n",
    "You can generate all $6$ with `itertools.permutations()` like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 102,
   "id": "8a3bb84d",
   "metadata": {},
   "outputs": [],
   "source": [
    "import itertools\n",
    "from trendlist import pows, rands\n",
    "\n",
    "def perms(s):\n",
    "    # return a list of all the permutations of the elements of s\n",
    "    # s can be any iterable\n",
    "    _perms = itertools.permutations(s)\n",
    "    _perms = [list(perm) for perm in _perms] # lists are easy to work with\n",
    "    return _perms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 103,
   "id": "33ee832d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[1, 2, 4], [1, 4, 2], [2, 1, 4], [2, 4, 1], [4, 1, 2], [4, 2, 1]]"
      ]
     },
     "execution_count": 103,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "perms(pows(3))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a3b02dc",
   "metadata": {},
   "source": [
    "`pows(3)` is a list, but $s$ can be a list, a tuple, or even a set."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a24540a8",
   "metadata": {},
   "source": [
    "Next, let's decompose each permutation into trends.\n",
    "The `trendlist` package offers efficient algorithms and useful data structures for these tasks,\n",
    "but for starters, we'll use a simple, brute-force approach from the same package.\n",
    "\n",
    "The data structures and their methods are discussed in a separate notebook, [The `trendlist` Package]()]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "id": "24dc28e6",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[2, 4], [1]]"
      ]
     },
     "execution_count": 56,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from simple.trendlist import trend_list\n",
    "trend_list([2, 4, 1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "id": "79c5c50d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[[1, 2, 4, 8]],\n",
       " [[1, 2, 8, 4]],\n",
       " [[1, 4, 2, 8]],\n",
       " [[1, 4, 8], [2]],\n",
       " [[1, 8], [2, 4]],\n",
       " [[1, 8], [4], [2]],\n",
       " [[2, 1, 4, 8]],\n",
       " [[2, 1, 8, 4]],\n",
       " [[2, 4, 1, 8]],\n",
       " [[2, 4, 8], [1]],\n",
       " [[2, 8], [1, 4]],\n",
       " [[2, 8], [4], [1]],\n",
       " [[4], [1, 2, 8]],\n",
       " [[4, 1, 8], [2]],\n",
       " [[4], [2, 1, 8]],\n",
       " [[4, 2, 8], [1]],\n",
       " [[4, 8], [1, 2]],\n",
       " [[4, 8], [2], [1]],\n",
       " [[8], [1, 2, 4]],\n",
       " [[8], [1, 4], [2]],\n",
       " [[8], [2, 1, 4]],\n",
       " [[8], [2, 4], [1]],\n",
       " [[8], [4], [1, 2]],\n",
       " [[8], [4], [2], [1]]]"
      ]
     },
     "execution_count": 57,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def all_trend_lists(s):\n",
    "    return [trend_list(perm) for perm in perms(s)]\n",
    "\n",
    "all_trend_lists(pows(4))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9294f0fd",
   "metadata": {},
   "source": [
    "Try `all_trend_lists()` with some, trendy enumerable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "876fe3bc",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "0e8d65b8",
   "metadata": {},
   "source": [
    "We've already mentioned, in passing, that the trend means in a trend list drop from left to right.\n",
    "Before going any farther, notice a couple of other things:\n",
    "\n",
    "1. Swapping any pair of adjacent trends gives you a new permutation that has exactly one fewer trend.\n",
    "If you have a trendlist with $n$ trends, $L = [T_1, T_2, ... T_{k-1}, T_k, T_{k+1}, T_{k+2}, ... T_n]$,\n",
    "with means $M(L) = [\\mu_1, \\mu_2, ... \\mu_n]$,\n",
    "and you swap $T_k$ and $T_{k+1}$, these trends will merge into a single trend, $T_x$.\n",
    "But because $\\mu_{k-1} > \\mu_k > \\mu_x > \\mu_{k+1} > \\mu_{k+2}$, \n",
    "fusing that pair just drops the number of trends by 1:\n",
    "$L2 = [T_1, T_2, ... T_{k-1}, T_x, T_{k+2}, ... T_n]$\n",
    "\n",
    "1. Rotating a trend from one end to the other decreases the number of trends, by *at least one*, but maybe more.\n",
    "In the trendlist $L$, if, instead of swapping a pair of neighbors, you rotate $T_n$ from the right end to the beginning, $T_n$ and $T_1$ will fuse into a single trend, $T_y$ because $\\mu_n$ < $\\mu_1$.  \n",
    "\n",
    "But you might not be done!\n",
    "\n",
    "You know that $\\mu_1 > \\mu_2$. But $\\mu_y$ is somewhere between $\\mu_1$ and $\\mu_n$. If it's a lot smaller than $\\mu_1$ then it might be smaller than $\\mu_2$, so you have to merge $T_y$ with $T_2$, and so on.\n",
    "\n",
    "In the example above, if you start with the permutation $p = [2, 8, 4, 1]$, decompose it into its trends $L = [[2, 8], [4], [1]]$, rotate $[1]$ to the beginning, and let the dust settle, you end up with a single trend: $L' = [[1, 2, 8, 4]]$\n",
    "\n",
    "In other words,\n",
    "\n",
    "* If you keep rotating trends from one end to the other, one at a time, each rotation will decrease the number of trends by *some* number, $>= 1$, until you can't rotate any more, because you finally have a single trend\n",
    "\n",
    "**Every sequence has exactly *one* circular permutation that is a single trend.**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e0a71a8",
   "metadata": {},
   "source": [
    "## Counting trends"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05aa8e4b",
   "metadata": {},
   "source": [
    "How many trend_lists does each trend_list have?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "id": "15566335",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1, 2, 1, 2, 2, 3]"
      ]
     },
     "execution_count": 59,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def count_trends(list_of_trend_lists):\n",
    "    # count the trends in each trend_list\n",
    "    return [len(trend_list) for trend_list in list_of_trend_lists]\n",
    "\n",
    "seq = pows(3)\n",
    "count_trends(all_trendlists(seq))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6134df1e",
   "metadata": {},
   "source": [
    "And how frequent is each of these counts?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "id": "2d7277d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "from collections import Counter\n",
    "\n",
    "counts = Counter(count_trends(all_trendlists(seq)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58f1fe38",
   "metadata": {},
   "source": [
    "With that done, you can report"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "id": "671be1c9",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "the number of permutations with exactly 0 trends is 0\n",
      "the number of permutations with exactly 1 trends is 2\n",
      "the number of permutations with exactly 2 trends is 3\n",
      "the number of permutations with exactly 3 trends is 1\n"
     ]
    }
   ],
   "source": [
    "for count in range(len(seq)+1):\n",
    "    print(f\"the number of permutations with exactly {count} trends is {counts[count]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d16c6291",
   "metadata": {},
   "source": [
    "Collections.Counter cheerfully reports $0$ for everything it doesn't know about,\n",
    "instead of giving you an error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "id": "0f35a880",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "the number of permutations with exactly 100 trends is 0\n"
     ]
    }
   ],
   "source": [
    "print(f\"the number of permutations with exactly 100 trends is {counts[100]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27e2445b",
   "metadata": {},
   "source": [
    "Before moving on, wrap that into a function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "id": "854216a7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 5040, 13068, 13132, 6769, 1960, 322, 28, 1]"
      ]
     },
     "execution_count": 63,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def n_trends(s):\n",
    "    # Return list telling how many permutations of s have exactly k trends.\n",
    "    # Include 0 at the beginning to say that no permutation has *no* trends.\n",
    "    # Python programmers like the first array index to be 0, not 1.\n",
    "    counts = [0]*(len(s) + 1)\n",
    "    for ntrends, count in Counter(count_trends(all_trendlists(s))).items():\n",
    "        counts[ntrends] = count\n",
    "    return counts\n",
    "\n",
    "n_trends(list(pows(8)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e97b6cc",
   "metadata": {},
   "source": [
    "## First, Not-Very-Deep Thoughts about `n_trends()`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "716281ff",
   "metadata": {},
   "source": [
    "You can already say a few things about `n_trends()`.\n",
    "\n",
    "1. As long as the argument is trendy, the returned list will only depend on the size of the arg.\n",
    "1. Every permutation has *some* number of trends, so it's in one of the buckets. This means the sum of the list `n_trends()` returns will be $n!$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "id": "5e93ffaf",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 66,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# trendy values don't matter, only their lengths\n",
    "n = 7\n",
    "r = list(rands(7))\n",
    "p = pows(7)\n",
    "n_trends(r) == n_trends(p)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 70,
   "id": "930f6d24",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sum of all buckets is n!: True\n"
     ]
    }
   ],
   "source": [
    "from math import factorial\n",
    "\n",
    "# n_trends(s) sums to len(s)!\n",
    "print(f\"sum of all buckets is n!: {sum(n_trends(r)) == factorial(7)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c02cec25",
   "metadata": {},
   "source": [
    "Also,\n",
    "\n",
    "3. Every list has exactly one circular permutation that's a single trend.\n",
    "This partitions the $n!$ permutations into non-overlapping subsets, each of size $n$, that can be rotated into the same single trend.\n",
    "\n",
    "Using our earlier example, the list [1, 2, 8, 4] has four circular permutations.\n",
    "$[1, 2, 8, 4]$\n",
    "\n",
    "$[4, 1, 2, 8]$\n",
    "\n",
    "$[8, 4, 1, 2]$\n",
    "\n",
    "$[2, 8, 4, 1]$\n",
    "\n",
    "\n",
    "Each can broken down into a trendlist.\n",
    "\n",
    "$[[1, 2, 8, 4]]$\n",
    "\n",
    "$[[4], [1, 2, 8]]$\n",
    "\n",
    "$[[8], [4], [1, 2]]$\n",
    "\n",
    "$[[2, 8], [4], [1]]$\n",
    "\n",
    "\n",
    "Exactly one of these has a lone trend.\n",
    "The number of permutations that are a single trend is, therefore, `n_trends()[1]` = $n!/n = (n-1)!$\n",
    "\n",
    "Let's try that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 73,
   "id": "ce0590b7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "number of single-trends is correct: True\n"
     ]
    }
   ],
   "source": [
    "# n_trends()[1] = (n-1)!\n",
    "print(f\"number of single-trends is correct: {n_trends(pows(n))[1] == factorial(n-1)}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b1f63fa",
   "metadata": {},
   "source": [
    "## Slightly Beyond the Basics with `n_trends()`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d0b3903",
   "metadata": {},
   "source": [
    "Clearly `n_trends()[0]` == $0$ and `n_trends()[1]` == $(n-1)!$, but why is `n_trends()[-1]` == $1$?\n",
    "\n",
    "That's easy enough. First, any sequence that's sorted from biggest to smallest will have exactly $n$ trends.\n",
    "The first number is bigger than the average of everything that follows it, so it's a trend of length $1$.\n",
    "The same argument holds, in turn, for each element in the sequence, making every element a trend of length $1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "id": "aca73314",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "20"
      ]
     },
     "execution_count": 75,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "r_20 = rands(20)\n",
    "len(trend_list(sorted(r_20, reverse=True)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af5096f3",
   "metadata": {},
   "source": [
    "Why can't other permutations also have $n$ trends?\n",
    "\n",
    "To have $n$ trends, each element must be its own trend.\n",
    "You know that in a trendlist, the trends are monotonically decreasing from left to right,\n",
    "so the elements in the original trend must also decrease monotonically,\n",
    "which means the sequence must be sorted, in decreasing order.\n",
    "\n",
    "In other words, there's one and only one sequence with $n$ trends."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa1643b9",
   "metadata": {},
   "source": [
    "A similar argument shows you `n_trends()[-2]` $== {n \\choose 2}$\n",
    "\n",
    "Here it is, step-by-step:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12882f9b",
   "metadata": {},
   "source": [
    "1. `n_trends()[-2]` is the number of permutations with exactly $n-1$ trends.\n",
    "1. To have exactly $n-1$ trends, exactly one trend must have 2 elements.\n",
    "1. There are $n \\choose 2$ ways to pick 2 elements out to put in that trend.\n",
    "1. The two-element trend can only be in increasing order: [smaller, larger]. Otherwise, it's not a trend.\n",
    "1. Once the elements are assigned to trends -- two to a 2-element trend in increasing order, one to each of the rest -- there is only one way to arrange the trends: with their means in decreasing order.\n",
    "\n",
    "In other words, by picking two elements, you define a unique permutation that decomposes into $n-1$ trends, with one of those trends made up of the two elements you picked, in increasing order.\n",
    "\n",
    "Check it out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 77,
   "id": "1c268a19",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 77,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from math import comb # binomial coefficients\n",
    "\n",
    "n_trends(pows(7))[-2] == comb(7, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d3e0f9e",
   "metadata": {},
   "source": [
    "## `n_trends()[k]`, the number of permutations with exactly `k` trends."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33288c4a",
   "metadata": {},
   "source": [
    "## `n_trends(s)[k]` is $len(s) \\brack k$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5bac329e",
   "metadata": {},
   "source": [
    "Is there a simple expression for the number of permutations of `s` with exactly `k` trends?\n",
    "Yes!\n",
    "\n",
    "Sort of.\n",
    "\n",
    "First, you have to learn about Stirling Numbers."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f6c4e33",
   "metadata": {},
   "source": [
    "## Stirling Numbers Count Cycles"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f7ca6a2e",
   "metadata": {},
   "source": [
    "A Sterling number of the first kind (unsigned), written $n \\brack k$, \n",
    "is the number of ways you can decompose *n* elements into *k* distinct cycles.\n",
    "\n",
    "What's that mean?\n",
    "\n",
    "A set of four elements, $\\{a, b, c, d\\}$, has these, distinct decompositions:\n",
    "\n",
    "* (a b c d)\n",
    "* (a b d c)\n",
    "* (a c b d)\n",
    "* (a c d b)\n",
    "* (a d b c)\n",
    "* (a d c b)\n",
    "* (a)(b c d)\n",
    "* (a)(b d c)\n",
    "* (b)(a c d)\n",
    "* (b)(a d c)\n",
    "* (c)(a b d)\n",
    "* (c)(a d b)\n",
    "* (d)(a b c)\n",
    "* (d)(a c b)\n",
    "* (a b)(c d)\n",
    "* (a c)(b d)\n",
    "* (a d)(b c)\n",
    "* (a)(b)(c d)\n",
    "* (a)(c)(b d)\n",
    "* (a)(d)(b c)\n",
    "* (b)(c)(a d)\n",
    "* (b)(d)(a c)\n",
    "* (c)(d)(a b)\n",
    "* (a)(b)(c)(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad038968",
   "metadata": {},
   "source": [
    "Any other decomposition will fall into one of these buckets.\n",
    "For example, $(d a b c)$ is not in the list above, but it's just a different way of writing the cycle $(a b c d)$ -- each is a circular permutation of the other.\n",
    "\n",
    "Similarly, ordering of adjacent cycles doesn't matter, so $(a b) (c d)$ is the same as $b(c d) (a b)$.\n",
    "\n",
    "If you count elements in the list above, you can see that there are 11 lines with exactly two cycles. $ {4 \\brack 2} == 11 $"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d6fdbdb",
   "metadata": {},
   "source": [
    "This is easy to see, with reasoning just like you used to show `n_trends(s)[-2]` $== {n \\choose 2}$\n",
    "\n",
    "1. Partition the $n$ elements of a trendy iterable into $k$ subsets.\n",
    "1. Consider each subset a cycle, and rotate it into the unique order that is a single trend.\n",
    "1. Order the trends in order of decreasing means.\n",
    "\n",
    "This is a 1-1 match of trendlists of permutations to decompositions of the set into distinct cycles, and vice-versa: a bijection.\n",
    "Each decomposition into $k$ cycles maps, reversably, to a decomposition of some permutation into $k$ trends. \n",
    "\n",
    "To find the number of ways to take $n$, random reals and write them as a single sequence of $k$ trends, just look up the value of $n \\brack k$ ."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e5c28fd",
   "metadata": {},
   "source": [
    "Stirling numbers were first studied by James Stirling [1692-1770], a crony of Sir Isaac Newton's. The numbers were named for him, not the other way around.\n",
    "\n",
    "They pop up in unexpected places. This is yet another. There isn't a simple, closed, algebraic expression for $n \\brack k$, but there isn't for or $sin(\\theta)$ or $ln(x)$ either, and you've gotten used to using sines and natural logs. *Stirling number* is just another well-defined function to add to your mathematical vocabulary.\n",
    "\n",
    "When you actually need a value for $n \\brack k$ , you just plug in the parameters and look it up, the way you would  𝑡𝑎𝑛(𝜃)  or a  𝑝 -value for a particular part of the normal distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 88,
   "id": "e22bf94a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Using n_trends(): [0, 5040, 13068, 13132, 6769, 1960, 322, 28, 1]\n"
     ]
    }
   ],
   "source": [
    "print(f\"Using n_trends(): {n_trends(pows(8))}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 78,
   "id": "14d8bb1e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Using Stirling numbers: [0, 5040, 13068, 13132, 6769, 1960, 322, 28, 1]\n"
     ]
    }
   ],
   "source": [
    "def stirlings(n):\n",
    "    # compute n_trends() with Stirling numbers\n",
    "    from sympy.functions.combinatorial.numbers import stirling # package for symbolic computation\n",
    "    row = []\n",
    "    for k in range(n+1):\n",
    "        row.append(stirling(n=n, k=k, kind=1))\n",
    "    return row\n",
    "print(f\"Using Stirling numbers: {stirlings(8)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "058af6fc",
   "metadata": {},
   "source": [
    "## Why Does This Help?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ce355fb",
   "metadata": {},
   "source": [
    "Okay, so it's the Sterling numbers (of the first kind, unsigned). So what?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4273221",
   "metadata": {},
   "source": [
    "First, there's a big performance difference."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 101,
   "id": "8023e314",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "n_trends(pows(1)): 6.53 µs ± 29.4 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n",
      "stirlings(1): 1.42 µs ± 4.4 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)\n",
      "n_trends(pows(2)): 27.7 µs ± 25.4 ns per loop (mean ± std. dev. of 7 runs, 10,000 loops each)\n",
      "stirlings(2): 2.11 µs ± 7.55 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n",
      "n_trends(pows(3)): 145 µs ± 87.6 ns per loop (mean ± std. dev. of 7 runs, 10,000 loops each)\n",
      "stirlings(3): 4.39 µs ± 9.61 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n",
      "n_trends(pows(4)): 884 µs ± 1.14 µs per loop (mean ± std. dev. of 7 runs, 1,000 loops each)\n",
      "stirlings(4): 5.72 µs ± 26.6 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n",
      "n_trends(pows(5)): 6.31 ms ± 8.51 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)\n",
      "stirlings(5): 6.32 µs ± 11.8 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "for n in range(1, 6):\n",
    "    print(f\"n_trends(pows({n})): \", end=\"\")\n",
    "    %timeit n_trends(pows(n))\n",
    "    print(f\"stirlings({n}): \", end=\"\")\n",
    "    %timeit stirlings(n)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7eae85f6",
   "metadata": {},
   "source": [
    "Don't even think about using `n_trends()` for larger *n*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 100,
   "id": "3071f03c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "16.1 µs ± 21.5 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit stirlings(20)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f22d475",
   "metadata": {},
   "source": [
    "Second, if we know a fact about Stirling numbers, we can apply it to trends.\n",
    "For example, we know that the average Stirling number, $n \\brack k$, is $H(n)$, \n",
    "the *n*th Harmonic number, $1 + 1/2 + 1/3 + ... + 1/n$\n",
    "\n",
    "In 1734, Leonard Euler showed that $\\lim\\limits_{n \\to \\inf}(H(n) - ln(n)) = \\gamma$,\n",
    "where $\\gamma = 0.5775664...$ is the Euler-Mascheroni constant.\n",
    "\n",
    "In other words, the average number of trends in a sequence of length $N$ is about $ln(N)$.\n",
    "\n",
    "If you decompose a sequence of a million random reals, one time in a million, you'll get a single trend, but on average, you'll get about $ln(10^6) = log_{10}(10^6)/log_{10}(e) \\approx 6/(7/16) = 96/7 \\approx 13.7$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a81cc1f7",
   "metadata": {},
   "source": [
    "## Counting Lengths of Longest Trends"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c7df07df",
   "metadata": {},
   "source": [
    "Let's count something else. If you list all permutations of a sequence, decomposed into trends, how long is the longest trend? We'll cannibalize the earlier code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 80,
   "id": "254f30d0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[3, 2, 3, 2, 2, 1]"
      ]
     },
     "execution_count": 80,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def longest_trend(trendlist):\n",
    "    return max([len(trend) for trend in trendlist])\n",
    "    \n",
    "def count_longest_trends(list_of_trendlists):\n",
    "    return [longest_trend(trendlist) for trendlist in list_of_trendlists]\n",
    "\n",
    "count_longest_trends(all_trendlists(pows(3)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 81,
   "id": "c7004644",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "the number of permutations that have a longest trend of length 0 trends is 0\n",
      "the number of permutations that have a longest trend of length 1 trends is 1\n",
      "the number of permutations that have a longest trend of length 2 trends is 3\n",
      "the number of permutations that have a longest trend of length 3 trends is 2\n"
     ]
    }
   ],
   "source": [
    "from collections import Counter\n",
    "\n",
    "counts = Counter(count_longest_trends(all_trendlists([1, 2, 4])))\n",
    "\n",
    "for count in range(len([1,2,4])+1):\n",
    "    print(f\"the number of permutations that have a longest trend of length {count} trends is {counts[count]}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 82,
   "id": "dfdd21a7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 1, 75, 200, 180, 144, 120]"
      ]
     },
     "execution_count": 82,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def l_trends(s):\n",
    "    # OEIS iiA126074\n",
    "    # Return list telling how many permutations of s have a longest trend of length l.\n",
    "    # Include 0 at the beginning to say that no permutation has *no* trends,\n",
    "    # because, Python programmers like the first array index to be 0, not 1.\n",
    "    counts = [0]*(len(s) + 1)\n",
    "    for ltrends, count in Counter(count_longest_trends(all_trendlists(s))).items():\n",
    "        counts[ltrends] = count\n",
    "    return counts\n",
    "\n",
    "l_trends(pows(6))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd650b1b",
   "metadata": {},
   "source": [
    "Once again, all sequences must have *some* longest trend, so the sum of `l_trends(s)` must be `factorial(len(s))`.\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 83,
   "id": "e23fd9e5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0, 1, 763, 5152, 8820, 8064, 6720, 5760, 5040]\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 83,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "print(l_trends(pows(8)))\n",
    "sum(l_trends(pows(8))) == factorial(8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 84,
   "id": "faa55cc1",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "411"
      ]
     },
     "execution_count": 84,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def sum_max_trend_lengths(s):\n",
    "    # OEIS A028418\n",
    "    sum = 0\n",
    "    for l_trend, count in enumerate(l_trends(s)):\n",
    "        sum += l_trend*count\n",
    "    return sum\n",
    "sum_max_trend_lengths(pows(5))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 86,
   "id": "280c435e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6620256696428571"
      ]
     },
     "execution_count": 86,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def mean_max_trend_length(s):\n",
    "    return sum_max_trend_lengths(s)/(factorial(len(s))*len(s))\n",
    "\n",
    "mean_max_trend_length(pows(8))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1138796e",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
