"""Unit-test getargs module."""
# TODO: How do I test --help?
# pylint: disable=too-many-arguments

import sys
from argparse import Namespace

import pytest

from trend_apps.getargs import getargs


@pytest.mark.parametrize(
    "args, debug, logging, sequence_length, trials, interval",
    [
        ("", False, False, 1000, 10, 2),
        ("-d", True, False, 1000, 10, 2),
        ("--debug", True, False, 1000, 10, 2),
        ("-l", False, True, 1000, 10, 2),
        ("--logging", False, True, 1000, 10, 2),
        ("-n 68", False, False, 68, 10, 2),
        ("--sequence_length 68", False, False, 68, 10, 2),
        ("-t 69", False, False, 1000, 69, 2),
        ("--trials 69", False, False, 1000, 69, 2),
        ("-i 10", False, False, 1000, 10, 10),
        ("--interval 10", False, False, 1000, 10, 10),
    ],
)
def test_getargs(args, debug, logging, sequence_length, trials, interval) -> None:
    """Test correct setting of flags and options, plus defaults."""
    sys.argv = ["pytest"] + args.split()
    args = getargs()
    assert args == Namespace(
        debug=debug,
        logging=logging,
        sequence_length=sequence_length,
        trials=trials,
        interval=interval,
    )


# pylint: disable=unused-argument
@pytest.mark.parametrize(
    "args, debug, logging, sequence_length, trials, interval",
    [
        ("--foozbaz", False, False, 1000, 10, 2),
    ],
)
def test_bad_args(
    args, debug, logging, sequence_length, trials, interval
) -> None:  # pylint: disable=too-many-arguments
    """Test handling bad args."""
    sys.argv = ["pytest"] + args.split()
    with pytest.raises(SystemExit):
        args = getargs()
    # TODO: see whether error message is correct
