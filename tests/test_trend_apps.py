"""There really should be tests here."""

from trend_apps import __version__


def test_version():
    """Correct version."""
    assert __version__ == "0.1.0"
